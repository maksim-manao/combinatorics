<?php

function file_log($header, $str, $footer){
    $result = date(DATE_ATOM)." $header $str\n$footer\n";
    error_log($result, 3 , "errors.log");
}
function display($header, $str, $footer){
    $result = "<b>$header</b> $str<br>$footer<br>";
    print_r($result);
}
function error_hendler($errno, $errstr, $errfile, $errline){
    if(!error_reporting() & $errno){
        return false;
    }

    $errstr = htmlspecialchars($errstr);
    $header = "";
    switch($errno){
        case E_USER_ERROR:
            $header = "ОШИБКА";
            break;
        case E_USER_WARNING:
            $header = "ПРЕДУПРЕЖДЕНИЕ";
            break;
        case E_USER_NOTICE:
            $header = "УВЕДОМЛЕНИЕ";
            break;
        default:
            $header = "Неизвестная ошибка";
            break;
    }
    $errstr = "[$errno] $errstr";
    $footer = "Ошибка в $errfile строка $errline";
    display($header, $errstr, $footer);
    file_log($header, $errstr, $footer);
    if($errno & E_USER_ERROR){
        exit(1);
    }
    else{
        return true;
    }
    
}
$old_error_hendler = set_error_handler("error_hendler");