<?php
class Permutation
{
    protected $n, $m, $count;
    private $regular, $used, $main;


    function __construct($n, $m){
        $this->n($n);
        $this->m($m);
        $this->call_errors();
        $this->creator(0);
    }


    function fact($value){
        if($value < 2) return 1;
        return $value * $this->fact($value - 1);
    }
    function lenght(){
        $n = $this->fact($this->count);
        $m = $this->fact($this->count - $this->m);
        return $n / $m;
    }



    private function creator($num){
        if($num == $this->m){
            // if(count($this->main) >= $this->lenght()){
            //     $this->main = [];
            // }
            $this->main[] = implode($this->regular);
        }
        for ($i=0; $i < $this->count; $i++) { 
            if(!$this->used[$i][1]){
                $this->used[$i][1] = 1;
                $this->regular[] = $this->used[$i][0];
                $this->creator($num + 1);
                $this->used[$i][1] = 0;
                array_pop($this->regular);
            }
        }
    }


    private function n($n){
        $this->n = $n;
        $this->count = strlen($n);
        $this->used = str_split($n);
        $this->regular = [];
        foreach ($this->used as $key => $value) {
            $this->used[$key] = [$value, 0];
        }
    }
    private function m($m){
        $this->m = $m;
    }
    function set($n, $m){
        $this->n($n);
        $this->m($m);
        $this->call_errors();
        $this->main = [];
        $this->creator(0);
    }
    function __set($name, $value){
        if($name == "n" || $name == "m"){
            $this->$name($value);
            $this->call_errors();
            $this->main = [];
            $this->creator(0);
        }
    }



    function print(){
        $count = count($this->main);
        echo "Строка ".$this->n." длина ".$this->m." количество $count<br>";
        foreach ($this->main as $value) {
            echo "$value<br>";
        }
    }
    function __toString(){
        return implode(" ",$this->main);
    }
    private function call_errors(){
        if($this->count < $this->m){
            $this->m = $this->count;
            trigger_error("Длина n меньше m. Теперь m равен длине n", E_USER_NOTICE);
        }
        if(!is_int($this->m)){
            trigger_error("m не число", E_USER_ERROR);
        }
    }
}
